import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  // state是数据源，所有vue组件需要存储的数据都统一放在state中
  state: {
    count: 0
  },
  // mutation用于变更state中的数据，可以在mutation中创建操作state数据的方法，mutation中的方法第一个参数为state，可以直接在其内编写修改state数据的逻辑
  mutations: {
    add(state) {
      state.count++
    },
    addN(state, step) {
      state.count+=step
    },
    sub(state) {
      state.count--
    },
    subN(state, step) {
      setTimeout(() => {
        state.count-=step
      }, 1000);
    }
  },
  /**
   * Action 用于处理异步任务。
   * 如果通过异步操作变更数据，必须通过 Action，而不能使用 Mutation，
   * 但是在 Action 中还是要通过触发Mutation 的方式间接变更数据
   * */ 
  actions: {
    addAsync(context) {
      setTimeout(() => {
        context.commit('add')
      }, 1000);
    },
    subNAsync(context, step) {
      setTimeout(() => {
        context.commit('subN', step)
      }, 1000);
    }
  },
  /**
   * Getter 用于对 Store 中的数据进行加工处理形成新的数据。
   */

  getters: {
    showNum(state) {
      return '当前最新的数量是【'+ state.count + '】'
    }
  }
})
